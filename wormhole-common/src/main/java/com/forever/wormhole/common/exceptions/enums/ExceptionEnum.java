package com.forever.wormhole.common.exceptions.enums;

import lombok.Getter;

/**
 * <p>
 * 异常信息枚举
 * </p>
 *
 * @author zhang yang ze
 * @date 2021-08-24 17:16
 * @since 1.0.0
 */
@Getter
public enum ExceptionEnum {
    /**
     * 默认
     */
    DEFAULT_EXCEPTION_MSG("5000", "发生未知错误"),

    OK("2000", "OK"),

    NET_ERROR("50001", "请求异常"),
    INVALID_REQUEST("50002", "无效请求"),

    TOKEN_INVALID("40001", "登录超时，请重新登录"),
    GRANT_INVALID("40002", "账号或密码不正确"),
    INSUFFICIENT_AUTHORITY("40002", "权限不足"),
    INVALID_SCOPE("40002", "权限不足"),
    UNAUTHORIZED_USE("40003", "用户未授权"),
    UNAUTHORIZED_CLIENT("40003", "客户端未授权"),
    ;

    /**
     * 错误码
     */
    private final String code;
    /**
     * 错误描述
     */
    private final String message;

    ExceptionEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
