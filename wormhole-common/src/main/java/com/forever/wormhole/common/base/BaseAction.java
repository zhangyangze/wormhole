package com.forever.wormhole.common.base;

import cn.hutool.core.util.StrUtil;
import com.forever.wormhole.common.exceptions.ServiceExceptException;
import com.forever.wormhole.common.exceptions.enums.ExceptionEnum;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.function.Supplier;

/**
 * <p>
 * 控制器公共类
 * </p>
 *
 * @author zhang yang ze
 * @date 2021-08-24 17:30
 * @since 1.0.0
 */
@Slf4j
public class BaseAction {

    /**
     * <p>
     * 公共返回操作
     * </p>
     *
     * @param req 请求
     * @param impl 实现方法
     * @return com.forever.wormhole.common.base.BaseResult<T>
     * @author zhang yang ze
     * @date 2021-08-25 16:20
     * @since 0.1.0
     */
    public <T> BaseResult<T> deal(HttpServletRequest req, Supplier<T> impl) {
        String sessionId = req.getRequestedSessionId();
        try {
            return BaseResult.success(sessionId, impl.get());
        } catch (ServiceExceptException e) {
            log.warn("业务异常 -> [{}]-{}", sessionId, e.getMessage());
            return BaseResult.fail(sessionId, e.getCode(), e.getMessage());
        } catch (IllegalArgumentException e) {
            log.warn("参数错误 -> [{}]-{}", sessionId, e.getMessage(), e);
            return BaseResult.fail(sessionId, ExceptionEnum.DEFAULT_EXCEPTION_MSG.getCode(), e.getMessage());
        } catch (Exception e) {
            log.warn("服务器异常 -> [{}]-{}", sessionId, e.getMessage(), e);
            return BaseResult.fail(sessionId, ExceptionEnum.DEFAULT_EXCEPTION_MSG.getCode(), ExceptionEnum.DEFAULT_EXCEPTION_MSG.getMessage());
        }
    }

    /**
     * <p>
     * 页面跳转
     * </p>
     *
     * @param url 路径
     * @return java.lang.String
     * @author zhang yang ze
     * @date 2021-08-25 16:10
     * @since 0.1.0
     */
    public String redirect(String url) {
        return StrUtil.format("redirect:{}", url);
    }

}
