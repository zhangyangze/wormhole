package com.forever.wormhole.common.base;

import cn.hutool.core.util.StrUtil;
import com.forever.wormhole.common.exceptions.enums.ExceptionEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.forever.wormhole.common.exceptions.enums.ExceptionEnum.OK;

/**
 * <p>
 * 公共返回结果
 * </p>
 *
 * @author zhang yang ze
 * @date 2021-08-24 17:30
 * @since 1.0.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseResult<T> {

    /**
     * 请求成功状态
     */
    private Boolean success;

    /**
     * 返回码
     */
    private String code;

    /**
     * 说明
     */
    private String message;

    /**
     * 返回数据
     */
    private T data;

    /**
     * 服务器当前时间戳
     */
    private Long ts = System.currentTimeMillis();

    /**
     * 请求ID
     */
    private String requestId;

    public BaseResult(Boolean success, String code, String message, T data, String requestId) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;
        this.requestId = requestId;
    }

    /**
     * <p>
     * 成功
     * </p>
     *
     * @param requestId 请求ID
     * @param data      返回数据
     * @return com.forever.wormhole.common.base.BaseResult<T>
     * @author zhang yang ze
     * @date 2021-08-25 15:56
     * @since 0.1.0
     */
    public static <T> BaseResult<T> success(String requestId, T data) {
        return new BaseResult<T>(true, OK.getCode(), OK.getMessage(), data, requestId);
    }

    /**
     * <p>
     * 成功
     * </p>
     *
     * @param requestId 请求ID
     * @return com.forever.wormhole.common.base.BaseResult<T>
     * @author zhang yang ze
     * @date 2021-08-25 15:56
     * @since 0.1.0
     */
    public static <T> BaseResult<T> success(String requestId) {
        return success(requestId, null);
    }

    /**
     * <p>
     * 失败
     * </p>
     *
     * @param requestId 请求id
     * @param type      错误类型
     * @return com.forever.wormhole.common.base.BaseResult<java.lang.String>
     * @author zhang yang ze
     * @date 2021-08-25 15:58
     * @since 0.1.0
     */
    public static <T> BaseResult<T> fail(String requestId, ExceptionEnum type, T data) {
        return new BaseResult<T>(false, type.getCode(), type.getMessage(), data, requestId);
    }

    /**
     * <p>
     * 失败
     * </p>
     *
     * @param requestId 请求id
     * @param type      错误类型
     * @return com.forever.wormhole.common.base.BaseResult<java.lang.String>
     * @author zhang yang ze
     * @date 2021-08-25 15:58
     * @since 0.1.0
     */
    public static <T> BaseResult<T> fail(String requestId, ExceptionEnum type) {
        return new BaseResult<T>(false, type.getCode(), type.getMessage(), null, requestId);
    }

    /**
     * <p>
     * 失败
     * </p>
     *
     * @param requestId 请求id
     * @param code      错误编码
     * @param msg       错误描述
     * @return com.forever.server.base.BaseResult<T>
     * @author zhang yang ze
     * @date 2021-01-13 11:24
     * @since 1.0.0
     */
    public static <T> BaseResult<T> fail(String requestId, String code, String msg) {
        return new BaseResult<T>(false, code, msg, null, requestId);
    }

}
