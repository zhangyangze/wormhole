package com.forever.wormhole.common.exceptions;

import com.forever.wormhole.common.exceptions.enums.ExceptionEnum;
import lombok.Getter;

/**
 * <p>
 * 自定义业务异常
 * </p>
 *
 * @author zhang yang ze
 * @date 2021-08-24 17:14
 * @since 1.0.0
 */
@Getter
public class ServiceExceptException extends RuntimeException {
    private static final long serialVersionUID = -2940237353947296638L;

    private final String code;

    public ServiceExceptException(String code, String message) {
        super(message);
        this.code = code;
    }

    public ServiceExceptException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public ServiceExceptException(ExceptionEnum e) {
        super(e.getMessage());
        this.code = e.getCode();
    }

    public ServiceExceptException(ExceptionEnum e, Throwable cause) {
        super(e.getMessage(), cause);
        this.code = e.getCode();
    }

    public ServiceExceptException(String message) {
        super(message);
        this.code = ExceptionEnum.DEFAULT_EXCEPTION_MSG.getCode();
    }

    public ServiceExceptException(String message, Throwable cause) {
        super(message, cause);
        this.code = ExceptionEnum.DEFAULT_EXCEPTION_MSG.getCode();
    }

}
