package com.forever.wormhole.user.controller;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p>
 *
 * </p>
 *
 * @author zhang yang ze
 * @date 2021-08-26 16:28
 * @since 1.0.0
 */
@SpringBootApplication
@EnableDiscoveryClient
public class UserApplication {
}
